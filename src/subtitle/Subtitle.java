/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package subtitle;

import java.awt.List;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author mozar
 */
public abstract class Subtitle {

    public static String formatDetik(long durasi) {
        long detik = durasi / 1000;
        long mili = durasi - (detik * 1000);
        return detik + "," + mili;
    }

    /**
     * @param args the command line arguments
     */
    String nama_file,
            extensi;
    ArrayList<Percakapan> arrayPercakapan;
    List listPercakapan;

    public static String[] validFile = new String[]{"srt"};

    public Subtitle() {
        nama_file = "";

    }

    public ArrayList<Percakapan> getArrayPercakapan() {
        return arrayPercakapan;
    }

    public static String getExtensiFile(String namaFile) {
        File f = new File(namaFile);
        namaFile = f.getName().toLowerCase();
        int indexExtensiFile = namaFile.lastIndexOf(".");
        if (indexExtensiFile >= 0) {
            String extensiFile = namaFile.substring(indexExtensiFile + 1);
            return extensiFile;
        }
        return "";
    }

    public static boolean adalahSubtitle(String namaFile) {
        String extensiFile = getExtensiFile(namaFile);
        System.out.println("extensi file = " + extensiFile);
        for (String extensi : validFile) {
            if (extensi.equals(extensiFile)) {
                return true;
            }
        }
        return false;
    }

    public static String getFileName(String path) {
        File f = new File(path);
        return f.getName();
    }

    public static String stringPaddingLeft(String teks, int length, char padWith) {
        if (padWith == 0) {
            return teks;
        }
        while (teks.length() < length) {
            teks = padWith + teks;
        }
        return teks;
    }

    abstract boolean adalah_waktu_subtitle(String teks);

    public abstract void initSubtitle();

    abstract void geserWaktu(int indexMulai, int indexAkhir, long delta);

    public abstract String[] getFormatedPercakapan();

    public abstract String getWaktuMulai(Percakapan p);

    public boolean adalah_angka(String teks) {
        teks = teks.trim();
        int p = teks.length();
        if (p == 0) {
            return false;
        }
        for (int i = 0; i < p; i++) {
            int c = teks.charAt(i);
            if (c >= 0 && c < 256) {
                if (c < '0' || c > '9') {
                    return false;
                }
            }
        }
        return true;
    }

    public void hapusPercakapan(int index) {
        if (index >= arrayPercakapan.size() || index < 0) {
        } else {
            arrayPercakapan.remove(index);
        }
    }
}
