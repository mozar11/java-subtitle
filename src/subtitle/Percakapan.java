/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package subtitle;

/**
 *
 * @author mozar
 */
public class Percakapan {

    private String waktu = "", nomor = "", dialog = "";
    private boolean waktu_init = false, nomor_init = false;
    private long waktuMulai_long = 0, waktuAkhir_long = 0;

    public boolean isWaktuInited() {
        return this.waktu_init;
    }

    public boolean isNomorInited() {
        return this.nomor_init;
    }

    public void setWaktu(String waktu, long waktuMulai, long waktuAkhir) {
        this.waktu = waktu;
        this.waktu_init = true;
        this.waktuMulai_long = waktuMulai;
        this.waktuAkhir_long = waktuAkhir;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
        this.nomor_init = true;
    }

    public void reset() {
        this.nomor = "";
        this.waktu = "";
        this.dialog = "";
        this.waktu_init = false;
        this.nomor_init = false;
    }

    public void setDialog(String d) {
        this.dialog += d + "\n";
    }

    public String getWaktu() {
        return waktu;
    }

    public String getDialog() {
        return dialog;
    }

    long getWaktuMulai() {
        return waktuMulai_long;
    }

    public long getDurasi() {
        return waktuAkhir_long-waktuMulai_long;
    }

}
