/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package subtitle;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author mozar
 */
public class SRT extends Subtitle {

    private final char[] format_waktu;
    //private final String nama_file;

    public SRT(String nama_file) {
        format_waktu = "nn:nn:nn,nnn --> nn:nn:nn,nnn".toCharArray();
        this.nama_file = nama_file;
        this.extensi = getExtensiFile(nama_file);
    }

    @Override
    protected boolean adalah_waktu_subtitle(String teks) {
        if (teks.length() != format_waktu.length) {
            return false;
        }
        int panjang = format_waktu.length;
        char[] teks_char = teks.toCharArray();
        for (int i = 0; i < panjang; i++) {
            if (format_waktu[i] == 'n') {
                if (teks_char[i] < '0' || teks_char[i] > '9') {
                    return false;
                }
            } else if (format_waktu[i] != teks_char[i]) {
                return false;
            }
        }
        return true;
    }

    /**
     * membaca semua percakapan dalam subtitle untuk tipe subtitle srt
     */
    @Override
    public void initSubtitle() {
        this.arrayPercakapan = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(this.nama_file)));
            String baris;
            Percakapan p = new Percakapan();
            while ((baris = br.readLine()) != null) {
                if (adalah_angka(baris)) {
                    if (p.isWaktuInited()) {
                        //masukkan ke dalam array, karena format satu percakapan sudah lengkap
                        this.arrayPercakapan.add(p);
                        p = new Percakapan();
                        p.setNomor(baris);
                    } else {
                        p.setNomor(baris);
                    }
                } else if (adalah_waktu_subtitle(baris)) {
                    char[] waktu = baris.toCharArray();
                    if (p.isWaktuInited()) {
                        this.arrayPercakapan.add(p);
                        p = new Percakapan();
                        p.setWaktu(baris, waktuToLong(waktu, 0), getWaktuAkhir(waktu));
                    } else {
                        p.setWaktu(baris, waktuToLong(waktu, 0), waktuToLong(waktu, 17));
                    }
                } else {
                    if (baris.trim().length() > 0) {
                        p.setDialog(baris);
                    }
                }
            }
            if (p.isNomorInited() && p.isWaktuInited()) {
                this.arrayPercakapan.add(p);
            }
            br.close();
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    @Override
    void geserWaktu(int indexMulai, int indexAkhir, long delta) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private long getWaktuMulai(char[] waktu) {
        if (waktu.length < 12) {
            return 0;
        }
        long jam = waktu[0] - '0';
        jam = (jam * 10) + waktu[1] - '0';
        long menit = waktu[3] - '0';
        menit = (menit * 10) + waktu[4] - '0';
        long detik = waktu[6] - '0';
        detik = (detik * 10) + waktu[7] - '0';
        long milidetik = waktu[9] - '0';
        milidetik = (milidetik * 10) + waktu[10] - '0';
        milidetik = (milidetik * 10) + waktu[11] - '0';
        detik = detik * 1000;
        menit = menit * 60 * 1000;
        jam = jam * 60 * 60 * 1000;
        return jam + menit + detik + milidetik;
    }

    private long getWaktuAkhir(char[] waktu) {
        if (waktu.length < 29) {
            return 0;
        }
        long jam = waktu[17] - '0';
        jam = (jam * 10) + waktu[18] - '0';
        long menit = waktu[20] - '0';
        menit = (menit * 10) + waktu[21] - '0';
        long detik = waktu[23] - '0';
        detik = (detik * 10) + waktu[24] - '0';
        long milidetik = waktu[26] - '0';
        milidetik = (milidetik * 10) + waktu[27] - '0';
        milidetik = (milidetik * 10) + waktu[28] - '0';
        detik = detik * 1000;
        menit = menit * 60 * 1000;
        jam = jam * 60 * 60 * 1000;
        return jam + menit + detik + milidetik;

    }

    private long waktuToLong(char[] waktu, int indexMulai) {
        if (waktu.length - indexMulai < 12) {
            return 0;
        }
        int i = indexMulai;
        long jam = waktu[i] - '0';
        jam = (jam * 10) + waktu[i + 1] - '0';
        long menit = waktu[i + 3] - '0';
        menit = (menit * 10) + waktu[i + 4] - '0';
        long detik = waktu[i + 6] - '0';
        detik = (detik * 10) + waktu[i + 7] - '0';
        long milidetik = waktu[i + 9] - '0';
        milidetik = (milidetik * 10) + waktu[i + 10] - '0';
        milidetik = (milidetik * 10) + waktu[i + 11] - '0';
        detik = detik * 1000;
        menit = menit * 60 * 1000;
        jam = jam * 60 * 60 * 1000;
        return jam + menit + detik + milidetik;

    }

    @Override
    public String[] getFormatedPercakapan() {
        String[] d = new String[arrayPercakapan.size()];
        int c = 0;
        for (Percakapan p : arrayPercakapan) {
            d[c++] = c + ") [" + p.getWaktu() + "] " + p.getDialog();
        }
        return d;
    }

    @Override
    public String getWaktuMulai(Percakapan p) {
        String waktu = p.getWaktu();
        if (waktu.length() > 11) {
            return p.getWaktu().substring(0, 12);
        } else {
            return "00:00:00,000";
        }
    }

}
